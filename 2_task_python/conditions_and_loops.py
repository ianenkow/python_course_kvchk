#1. Выведите все символы из строки «Программисты — не волшебники, способные вернуть удалённые в
#   прошлом году фотографии, взломать за 5 секунд любой сайт и починить микроволновку. Но именно эти
#   люди создают то пространство, где и появляются мемы про них», значения индексов которых делятся
#   на 10.

S = '«Программисты — не волшебники, способные вернуть удалённые в прошлом году фотографии,'\
    'взломать за 5 секунд любой сайт и починить микроволновку. Но именно эти люди создают то'\
    'пространство, где и появляются мемы про них»'

indice = 0
for char in S:
    if indice % 10 == 0:
        print(char)
    indice += 1

print()


#2. Напишите скрипт по проверки условия: если число больше 20 и меньше 40 - вывести “YES”, иначе
#  “NO”.

I = 30

if 20 < I < 40:
    print('YES \n')
else:
    print('NO \n')



#3. Выведите числа из диапазона от 1 до 10 двумя способами, используя циклы for и while.


for integer in range(1, 11):
    print(integer)
print()

n = 1
while n < 11:
    print(n)
    n += 1
print()


#4. Посчитайте количество вхождений элемента со значением «3» тремя способами в сле­дующем списке:
#   [3 0 1 3 0 4 3 3 4 5 6 6 1 3], используя цикл for, while и метод count.

L = [3, 0, 1, 3, 0, 4, 3, 3, 4, 5, 6, 6, 1, 3]

#count
print(L.count(3))

#for
n = 0
for char in L:
    if char == 3:
        n += 1
print(n)

#while
n = 0
l = 0
while l < len(L):
    if L[l] == 3:
        n += 1
    l += 1
print(n)
print()


#5. Напишите программу, выводящую среднее из десяти значений.

L = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

result = sum(L)/len(L)
print(result)

result = 0
s = 0
for integer in L:
    s += integer
result = s/10
print(result)
print()


#6. Выведите числа из диапазона от -20 до 20 с шагом 3 двумя способами, используя цикл for и while.

#for
for integer in range(-20, 21, 3):
    print(integer)

#while
n = -20
iteration = 0
while n < 21:
    if iteration % 3 == 0:
        print(n)
    n += 1
    iteration += 1
print()


#7. Напишите программу, которая считывает целое число (месяц), после чего выводит сезон к которому
#   этот месяц относится.

while True:
    print('Введите номер месяца для определения сезона или "Выход" для выхода, и нажмите ENTER')

    month_input = input()
    

    try:
        if month_input == 'Выход' or month_input == 'выход':
            break
        
        month_input = int(month_input)

        if month_input == 12 or (month_input > 0 and month_input < 3) :
            season = 'winter'
        elif month_input > 2 and month_input < 6:
            season = 'spring'
        elif month_input > 5 and month_input < 9:
            season = 'summer'
        elif 9 <= month_input < 12:
            season = 'autumn'

        print(season)
        
    except:
        print('Не удалось распознать номер месяца, попробуйте ещё раз')


    
