#8. Напишите функцию, вычисляющую максимальное из трех чисел. Два вызова функции.

def findMax(x, y, z):
    L = [x, y, z]
    L.sort(reverse=True)
    return L[0]

print(findMax(1, 20, 3))
print(findMax(-3, -5, -20))
print()


#9. Напишите функцию, которая возвращает инвертированную строку, передаваемую ей на вход.

def invertString(line):
    L = list(line)
    l = line[::-1]
    return ''.join(l)

print(invertString('It could be fun but it will not'))
print()    


#10. Напишите функцию для вычисления факториала задаваемого числа. Два вызова функции.

def calcFactorial(integer):
    if type(integer) != int or integer < 1:
        return 'Вычисление факториала возможно только для целых положительных чисел'
    else:
        if integer == 1:
            return integer
        else:
            return integer * calcFactorial(integer-1)

print(calcFactorial(6))
print(calcFactorial(2.4))
print()


#11. Напишите функцию, удаляющую повторяющиеся элементы в списке. Три вызова функции.

def delRepeats(sth):    
    l = []
        
    for element in sth:
        if l.count(element) > 0:
            continue
        else:
            l.append(element)
    
    return l


L1 = [1, 2, 3, 14, 2, 0, 19, 26, 288, 11, 14, 15, 0, 8, 2, 2, 3]
L2 = list('Humpty-dumpty sat on Great wall...')
L3 = ['a', 3, -8, {'h1':'n1'}, 'b', '4', 4, 3, 12, {'h1':'n1'}, 'a']

print(delRepeats(L1))
print(delRepeats(L2))
print(delRepeats(L3))
print()


#12. Напишите функцию, которая проверяет, является ли строка палин­дромом (читается одинаково как
#    слева направо, так и наоборот). Два вызова функции (с палиндромом и нет).

def isPalindrome(line):
    
    newLine = str(line).replace(' ', '')
    newLine = newLine.lower()
    invertLine = invertString(newLine)
    
    if newLine == invertLine:
        answer = 'YES'
    else:
        answer = 'NO'
    return answer

print(isPalindrome('Лазер Боре хер обрезал'))
print(isPalindrome('Палиндром иначе называется "рачья песнь"'))
print()


#13. Декорируйте функцию из первого упражнения таким образом, чтобы возвращаемое ей значение
#    возводилось в квадрат.

def double_decorator(func):
    def inner(x, y, z):
        return func(x, y, z) ** 2
    return inner

@double_decorator
def findMax(x, y, z):
    L = [x, y, z]
    L.sort(reverse=True)
    return L[0]

print(findMax(1, 4, 3))
print()


#14. Декорируйте функцию из 9 задания таким образом, чтобы возвращалась строка в верхнем регистре.

def upper_decorator(func):
    def inner(line):
        upper_line = str(func(line))
        return upper_line.upper()
    return inner

@upper_decorator
def invertString(line):
    L = list(line)
    l = line[::-1]
    return ''.join(l)

s = "And here's grand finale"
print(invertString(s))