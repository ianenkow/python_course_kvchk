#13. Добавьте еще несколько пар «ключ: значение» в следующий словарь: {0: 10, 1: 20}.

D = {0: 10, 1: 20}

D[2] = 30
D.update({3: 40, 4: 50})
print(D)

#14. Напишите скрипт, который из трех словарей создаст один новый.

D1 = {0: 10, 1: 20}
D2 = {2: 30, 3: 40}
D3 = {4: 50, 5: 60}

d = {**D1, **D2}
D = d | D3
print(D)


#15. Напишите скрипт для удаления элемента словаря.

D = {0: 10, 1: 20}
del D[0]
print(D)

#16. Напишите скрипт, проверяющий, существует ли заданный ключ в словаре.

D = {0: 10, 1: 20}

if 'spam' in D:
    print('Ключ существует')
else:
    print('Ключ не существует')


#Объявите и инициализируйте словарь различными способами.

D1 = {1: 'yes', 2: 'no'}

D2 = dict([(1, 'yes'), (2, 'no')])

L1 = [1, 2]
L2 = ['yes', 'no']
D3 = dict(zip(L1, L2))

print(D1)
print(D2)
print(D3)


#Напишите скрипт для удаления элемента словаря.

D = {0: 'yes', 1: 'no', 2: 'cancel'}

key = 0

del D[key]
print(D)


#Напишите скрипт, который выводит максимальное и минимальное числа среди значений словаря.


D = {'a': 0, 'b': 1, 'c': 2, 'd': 3, 'e': 4, 'f': 5, 'g': 89, 'h': 123, 'i': 43, 'j': -123, 'k': 423}

minElem = 0
maxElem = 0

for value in D.values():
    if value < minElem:
        minElem = value
    if value > maxElem:
        maxElem = value

print(minElem, maxElem)

