from dataclasses import replace
from gettext import find
import random
from typing import Counter

#1. Напишите скрипт, вычисляющий двумя способами длину строки.

S = "Hello world!"

x = len(S)
print(x)

x = 0
for letter in S:
    x += 1
print(x)

#2. Напишите скрипт, который заменяет произвольный символ/букву в строке на «$».

S = "Join kvchk automation team!"
x = random.randint(0, S.index(S[-1]))
L = list(S)
L[x] = '$'
S = ''.join(L)
print(S)


#3. Напишите скрипт, который позволяет из строки собрать другую по следующим правилам: новая строка
#   должна состоять из двух первых и двух последних элементов исходной строки. Если длина исходной
#   строки меньше двух, то результатом будет пустая строка.

S = "Never gonna give you up"

if len(S) == 2:
    x = S[:2] + S[-2:]
    print(x)
else:
    print()

#4. Напишите скрипт, который позволяет инвертировать последователь­ность элементов в строке.

S = "I tried palyndrom here, but in didn't work"

L = list(S)
L.reverse()
x = ''.join(L)
print(x)

#5. Напишите скрипт, выводящий все элементы строки с их номерами индексов.

S = "Can u count to infinity?"

for indice, char in enumerate(S):
    print(indice, char)

#6. Поменяйте регистр элементов строки на противоположный.

S = "Kisses To All In This Chyatik"

print(S.swapcase())

N = ''
for char in S:
    if char.isupper():
        N += char.lower()
    elif char.islower():
        N += char.upper()
    else:
        N += char
print(N)


#7. Выведите символ, который встречается в строке чаще, чем остальные.

S = "Chuck Norris can count to infinity... twice"

x = Counter(S).most_common(1)[0][0]
if x == ' ':
    print('  (пробел)')
else:
    print(x)

#Напишите скрипт, который считает количество вхождений символа в строку. Например: «google.com» —
#{'о': 3, 'g': 2,1, 'е': 1, Т: 1, 'm': 1, 'с': 1}

S = 'google.com'

L = list(S)
D = {}

for x in L:
    if x in D:
        D[x] += 1
    else:
        D[x] = 1
print(D)

#Напишите скрипт, позволяющий из исходной строки собрать две новые. Первая строка должна состоять только из элементов с нечетными индексами исходной строки, а вторая — с четными.

S = "The very long string to demonstrate 2 long substrings"
L = list(S)

s1 = ''
s2 = ''
x = 0

for char in L:
    if x % 2 == 0:
        s1 = s1 + char
    else:
        s2 = s2 + char
    x += 1
print(s1)
print(s2)


#Напишите скрипт, который удаляет задаваемый произвольный символ в строке.

S = 'Catch me doing it'
L = list(S)

char_to_delete = 'a'
S = S.replace(char_to_delete, '')
print(S)

#Напишите скрипт, проверяющий, содержится ли произвольный символ (элемент) или слово в строке

S = "А роза упала на лапу Азора"
alphabet = list('АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя')

x = random.randint(0, len(alphabet) - 1)
y = alphabet[x]

if y in S:
    print('Есть такая буква! {}'.format(alphabet[x]))
else:
    print('Нет такой буквы, вращайте барабан! {}'.format(alphabet[x]))