#17. Объявите и инициализируйте кортеж различными способами, после чего распакуйте его.

T1 = tuple('apple')
T2 = 1, 'xiaomi', 3
T3 = ('s', 'a', 'm', 's', 'u', 'n', 'g')

(x, i, a, o, m) = T1

print(x, i, a, o, m)

#18. Конвертируйте список в кортеж, затем добавьте в кортеж новые элементы.

L = [0, 1, 2, 3]

T = tuple(L)
l = list(T)
l.extend([4, 5])
T = tuple(l)
print(T)

#19. Конвертируйте кортеж в словарь.

T = ('name', 'Ivan'), ('surname', 'Anenkov')
D = dict((x, y) for x, y in T)
print(D)

#20. Напишите скрипт, подсчитывающий количество элементов типа кортеж в списке.

L = [(0, 1, 2), 'Oklahoma', 2.5, ('Hello', 'world')]

i = 0
for x in L:
    if type(x) is tuple:
        i += 1
print(i)



