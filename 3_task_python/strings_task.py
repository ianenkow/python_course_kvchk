
from modules import int_and_string_functions

#1. Напишите скрипт, вычисляющий двумя способами длину строки.

S = "Hello world!"

print(int_and_string_functions.calculate_string_length_first_way(S))
print(int_and_string_functions.calculate_string_length_second_way(S))


#2. Напишите скрипт, который заменяет произвольный символ/букву в строке на «$».

S = "Join kvchk automation team!"

print(int_and_string_functions.change_random_letter_for_dollar(S))


#3. Напишите скрипт, который позволяет из строки собрать другую по следующим правилам: новая строка
#   должна состоять из двух первых и двух последних элементов исходной строки. Если длина исходной
#   строки меньше двух, то результатом будет пустая строка.

S = "Never gonna give you up"

print(int_and_string_functions.make_new_string_with_two_first_and_two_last(S))


#4. Напишите скрипт, который позволяет инвертировать последователь­ность элементов в строке.

S = "I tried palyndrom here, but in didn't work"

print(int_and_string_functions.reverse_string(S))


#5. Напишите скрипт, выводящий все элементы строки с их номерами индексов.

S = "Can u count to infinity?"

int_and_string_functions.print_elements_and_indices_from_string(S)


#6. Поменяйте регистр элементов строки на противоположный.

S = "Kisses To All In This Chyatik"

print(int_and_string_functions.swap_case_new(S))


#7. Выведите символ, который встречается в строке чаще, чем остальные.

S = "Chuck Norris can count to infinity... twice"

print(int_and_string_functions.find_most_frequent_symbol(S))


#Напишите скрипт, который считает количество вхождений символа в строку. Например: «google.com» —
#{'о': 3, 'g': 2,1, 'е': 1, Т: 1, 'm': 1, 'с': 1}

S = 'google.com'

print(int_and_string_functions.count_symbol_repeats(S))


#Напишите скрипт, позволяющий из исходной строки собрать две новые. Первая строка должна состоять только из элементов с нечетными индексами исходной строки, а вторая — с четными.

S = "The very long string to demonstrate 2 long substrings"

print(int_and_string_functions.divide_string_for_honest_and_oss(S)[0])
print(int_and_string_functions.divide_string_for_honest_and_oss(S)[1])


#Напишите скрипт, который удаляет задаваемый произвольный символ в строке.

S = 'Catch me doing it'

print(int_and_string_functions.delete_chosen_char(S, 'a'))

#Напишите скрипт, проверяющий, содержится ли произвольный символ (элемент) или слово в строке

S = "А роза упала на лапу Азора"

print(int_and_string_functions.check_string_is_in_string(S, 'ро'))
