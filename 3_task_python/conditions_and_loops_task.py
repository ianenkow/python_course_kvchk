from modules import int_and_string_functions, list_functions

#1. Выведите все символы из строки «Программисты — не волшебники, способные вернуть удалённые в
#   прошлом году фотографии, взломать за 5 секунд любой сайт и починить микроволновку. Но именно эти
#   люди создают то пространство, где и появляются мемы про них», значения индексов которых делятся
#   на 10.

S = '«Программисты — не волшебники, способные вернуть удалённые в прошлом году фотографии,'\
    'взломать за 5 секунд любой сайт и починить микроволновку. Но именно эти люди создают то'\
    'пространство, где и появляются мемы про них»'

print(int_and_string_functions.choose_every_tenth_char(S))


#2. Напишите скрипт по проверки условия: если число больше 20 и меньше 40 - вывести “YES”, иначе
#  “NO”.

I = 30

print(int_and_string_functions.yes_or_no(I))


#3. Выведите числа из диапазона от 1 до 10 двумя способами, используя циклы for и while.

int_and_string_functions.print_numbers_from_range_with_for(1, 10)

int_and_string_functions.print_numbers_from_range_with_while(1, 10)


#4. Посчитайте количество вхождений элемента со значением «3» тремя способами в сле­дующем списке:
#   [3 0 1 3 0 4 3 3 4 5 6 6 1 3], используя цикл for, while и метод count.

L = [3, 0, 1, 3, 0, 4, 3, 3, 4, 5, 6, 6, 1, 3]

print(list_functions.count_repeating_elements_with_count_function(L, 3))

print(list_functions.count_repeating_elements_with_for_loop(L, 3))

print(list_functions.count_repeating_elements_with_while_loop(L, 3))

print()


#5. Напишите программу, выводящую среднее из десяти значений.

L = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

print(list_functions.calculate_average_value_from_list(L))
print()

#6. Выведите числа из диапазона от -20 до 20 с шагом 3 двумя способами, используя цикл for и while.

int_and_string_functions.print_numbers_from_range_with_for(-20, 20, 3)

int_and_string_functions.print_numbers_from_range_with_while(-20, 20, 3)


#7. Напишите программу, которая считывает целое число (месяц), после чего выводит сезон к которому
#   этот месяц относится.

int_and_string_functions.identify_season_with_month()


    
