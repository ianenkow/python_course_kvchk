def add_to_dictionary(dictionary1: dict, what_to_add):
    dictionary1.update(what_to_add)


def combine_dictionaries(*dictionaries):
    d = {}
    for dictionary in dictionaries:
        d = d | dictionary
    return d


def delete_element(dictionary1 : dict, key_to_delete):
    del dictionary1[key_to_delete]


def check_dictionary_has_key(dictionary1, key):
    if key in dictionary1:
        return 'Ключ существует'
    else:
        return 'Ключ не существует'


def find_min_and_max_value(dictionary1: dict):
    minElem = 0
    maxElem = 0

    for value in dictionary1.values():
        if value < minElem:
            minElem = value
        if value > maxElem:
            maxElem = value

    return [minElem, maxElem]


def delete_element_from_set(set1: set, element):
    set1.remove(element)


def calculate_set_length_first_way(set1: set):
    return len(set1)


def calculate_set_length_second_way(set1: set):
    counter = 0
    for element in set1:
        counter += 1
    return counter


def check_set_has_element(set1, element):
    if element not in set1:
        return 'Не входит'
    else:
        return 'Входит'


def add_element_to_set(set1: set, element):
    set1.add(element)


def unite_sets(*sets):
    result_set = set()
    for each_set in sets:
        result_set.update(each_set)
    return result_set


def add_elements_to_tuple(tuple1: tuple, *elements):
    tuple1 = list(tuple1)
    tuple1.extend(list(elements))
    tuple1 = tuple(tuple1)
    return tuple1


def convert_tuple_to_dictionary(tuple1):
    tuple1 = dict((x, y) for x, y in tuple1)
    return tuple1
        


    
