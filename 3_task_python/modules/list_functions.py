
import copy


def list_sum_first_way(list1):
    sum = 0
    for element in list1:
        sum += element
    return sum


def list_sum_second_way(list1):
    return sum(list1)


def multiply_every_element_by_number(list1, number):
    return [element * number for element in list1]


def lists_concat_first_way(list1, list2):
    return list1 + list2


def lists_concat_second_way(list1, list2):
    return [*list1, *list2]


def lists_concat_third_way(list1: list, list2):
    return list1.extend(list2)


def change_element_from_index(list1, index):
    if index == len(list1) - 1:
        print('Выбран последний элемент списка, невозможно выполнить задание')
    else:
        x = list1[index]
        list1[index] = list1[index+1]
        list1[index+1] = x
        return list1


def get_int_from_list(list1):
    S = ''
    for element in list1:
        if type(element) != int:
            S = f'Элемент "{element}" не является целочисленным, операция прервана'
            break
    else:
        S += str(element)
    return S


def find_min_and_max(list1):
    maxElem = sorted(list1)[-1]
    minElem = sorted(list1)[0]
    return f'max: {maxElem}, min: {minElem}'


def delete_repeating_elements(list1):
    return list(set(list1))


def copy_list_first_way(list1):
    list2 = copy.copy(list1)
    return list2


def copy_list_second_way(list1):
    list2 = [*list1]
    return list2


def convert_list_to_tuple(list1):
    list1 = tuple(list1)
    return list1


def count_tuples_in_list(list1):
    counter = 0
    for element in list1:
        if type(element) is tuple:
            counter += 1
    return counter


def count_repeating_elements_with_count_function(list1: list, element):
    #count
    return list1.count(element)


def count_repeating_elements_with_for_loop(list1, element):
    #for
    counter = 0
    for char in list1:
        if char == element:
            counter += 1
    return counter


def count_repeating_elements_with_while_loop(list1, element):
    #while
    element_counter = 0
    length_counter = 0
    while length_counter < len(list1):
        if list1[length_counter] == element:
            element_counter += 1
        length_counter += 1
    return element_counter


def calculate_average_value_from_list(list1: list):
    average = sum(list1)/len(list1)
    return average


def del_repeats(sth):
    l = []

    for element in sth:
        if l.count(element) > 0:
            continue
        else:
            l.append(element)

    return l

