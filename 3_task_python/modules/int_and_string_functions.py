
import random
from typing import Counter


def calculate_string_length_first_way(string1):
    return len(string1)


def calculate_string_length_second_way(string1):
    length = 0
    for letter in string1:
        length += 1
    return length


def change_random_letter_for_dollar(string1):
    x = random.randint(0, string1.index(string1[-1]))
    L = list(string1)
    L[x] = '$'
    S = ''.join(L)
    return S


def make_new_string_with_two_first_and_two_last(string1):
    length = calculate_string_length_first_way(string1)
    if length >= 2:
        x = string1[:2] + string1[-2:]
        return x
    else:
        return ''


def reverse_string(string1):
    return string1[::-1]


def print_elements_and_indices_from_string(string1):
    indice = 0
    for element in string1:
        print(f'{indice} {element}')
        indice += 1


def swap_case_new(string1: str):
    new_string = ''
    for char in string1:
        if char.isupper():
            new_string += char.lower()
    else:
            new_string += char.upper()
    return new_string


def find_most_frequent_symbol(string1: str):
    x = Counter(string1).most_common(1)[0][0]
    if x == ' ':
        return'  (пробел)'
    else:
        return x
        
        
def count_symbol_repeats(string1):
    L = list(string1)
    D = {}

    for symbol in string1:
        if symbol in D:
            D[symbol] += 1
        else:
            D[symbol] = 1
    return D


def divide_string_for_honest_and_oss(string1):
    L = list(string1)
    s1 = ''
    s2 = ''
    x = 0

    for char in L:
        if x % 2 == 0:
            s1 = s1 + char
        else:
            s2 = s2 + char
        x += 1
    
    return [s1, s2]


def delete_chosen_char(string1 : str, char_to_delete):
    string1 = string1.replace(char_to_delete, '')
    return string1


def check_string_is_in_string(string_to_check, what_to_check_for):
    if what_to_check_for in string_to_check:
        return(f'"{what_to_check_for}" есть в "{string_to_check}", отличная работа!')
    else:
        return(f'"{what_to_check_for}" нет в "{string_to_check}", вращайте барабан!')


def choose_every_tenth_char(string1):
    indice = 0
    result = []
    for char in string1:
        if indice % 10 == 0:
            result.extend(char)
        indice += 1
    return result


def invert_string(line):
    L = list(line)
    l = line[::-1]
    return ''.join(l)


def is_palindrome(line):

    newLine = str(line).replace(' ', '')
    newLine = newLine.lower()
    invertLine = invert_string(newLine)
    
    if newLine == invertLine:
        answer = f'Да, "{line}" является палиндромом'
    else:
        answer = f'Нет, "{line}" - не палиндром'
    return answer


def yes_or_no(int1):
    if 20 < int1 < 40:
        return 'YES \n'
    else:
        return 'NO \n'


def print_numbers_from_range_with_for(min_value, max_value, step=1):
    for integer in range(min_value, max_value + 1, step):
        print(integer)
    print()


def print_numbers_from_range_with_while(min_value, max_value, step=1):
    counter = min_value
    while counter <= max_value:
        print(counter)
        counter += step
    print()


def identify_season_with_month():
    while True:
        print('Введите номер месяца для определения сезона или "Выход" для выхода, и нажмите ENTER')

        month_input = input()
        
        try:
            if month_input == 'Выход' or month_input == 'выход':
                break
            
            month_input = int(month_input)

            if month_input == 12 or (month_input > 0 and month_input < 3) :
                season = 'winter'
            elif month_input > 2 and month_input < 6:
                season = 'spring'
            elif month_input > 5 and month_input < 9:
                season = 'summer'
            elif 9 <= month_input < 12:
                season = 'autumn'

            print(season)
            
        except:
            print('Не удалось распознать номер месяца, попробуйте ещё раз')


def find_max(*args):    
    L = [*args]
    L.sort(reverse=True)
    return L[0]


def calc_factorial(integer):
    if type(integer) != int or integer < 1:
        return 'Вычисление факториала возможно только для целых положительных чисел'
    else:
        if integer == 1:
            return integer
        else:
            return integer * calc_factorial(integer-1)

    
