from modules import list_functions, other_functions

#17. Объявите и инициализируйте кортеж различными способами, после чего распакуйте его.

T1 = tuple('apple')
T2 = 1, 'xiaomi', 3
T3 = ('s', 'a', 'm', 's', 'u', 'n', 'g')

(x, i, a, o, m) = T1

print(x, i, a, o, m)


#18. Конвертируйте список в кортеж, затем добавьте в кортеж новые элементы.

L = [0, 1, 2, 3]

L = list_functions.convert_list_to_tuple(L)
L = other_functions.add_elements_to_tuple(L, 4, 5, 6)
print(L)


#19. Конвертируйте кортеж в словарь.

T = ('name', 'Ivan'), ('surname', 'Anenkov')

T = other_functions.convert_tuple_to_dictionary(T)
print(T)


#20. Напишите скрипт, подсчитывающий количество элементов типа кортеж в списке.

L = [(0, 1, 2), 'Oklahoma', 2.5, ('Hello', 'world')]

print(list_functions.count_tuples_in_list(L))



