from modules import other_functions

#13. Добавьте еще несколько пар «ключ: значение» в следующий словарь: {0: 10, 1: 20}.

D = {0: 10, 1: 20}

other_functions.add_to_dictionary(D, {3: 40, 4: 50})
print(D)


#14. Напишите скрипт, который из трех словарей создаст один новый.

D1 = {0: 10, 1: 20}
D2 = {2: 30, 3: 40}
D3 = {4: 50, 5: 60}

print(other_functions.combine_dictionaries(D1, D2, D3))


#15. Напишите скрипт для удаления элемента словаря.

D = {0: 10, 1: 20}

other_functions.delete_element(D, 0)
print(D)


#16. Напишите скрипт, проверяющий, существует ли заданный ключ в словаре.

D = {0: 10, 1: 20}

print(other_functions.check_dictionary_has_key(D, '10'))


#Напишите скрипт, который выводит максимальное и минимальное числа среди значений словаря.

D = {'a': 0, 'b': 1, 'c': 2, 'd': 3, 'e': 4, 'f': 5, 'g': 89, 'h': 123, 'i': 43, 'j': -123, 'k': 423}

elems = other_functions.find_min_and_max_value(D)

print(f'Минимальное значение: {elems[0]}, максимальное: {elems[1]}')
