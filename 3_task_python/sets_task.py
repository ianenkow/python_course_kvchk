from modules import list_functions, other_functions

#21. Объявите и инициализируйте множество тремя различными способами.
'''
Тут вроде бы не имеет смысла отдельные функции писать
'''
S = {'s', 'e', 't'}
print(S)

L = [1, 2, 3, 4]
S = {*L}
print(S)

S = set('world')
print(S)


#22. Удалите элемент из множества.

S = {0, 1, 2, 3, 4}

other_functions.delete_element_from_set(S, 1)
print(S)


#23. Удалите повторяющиеся элементы из списка.

L = list('Hello world!')

L = list_functions.delete_repeating_elements(L)
print(L)


#24. Напишите скрипт, определяющий длину множества двумя различными способами.

S = {0, 1, 2, 3}

print(other_functions.calculate_set_length_first_way(S))
print(other_functions.calculate_set_length_second_way(S))


#25. Напишите скрипт для проверки, входит ли элемент в множество.

S = {0, 1, 2, 3}

print(other_functions.check_set_has_element(S, 'элемент'))

   
#Добавьте элемент в множество.

S = {1, 0, 3}

other_functions.add_element_to_set(S, 'elem')
print(S)

#Напишите скрипт для объединения двух множеств.

S1 = {0, 1, 2}
S2 = {'yes', 'no', 'maybe'}

print(other_functions.unite_sets(S1, S2))

