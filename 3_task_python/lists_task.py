
from modules import list_functions

#8. Вычислите сумму элементов (чисел) в списке двумя разными спосо­бами.

L = [1, 2, 3]

print(list_functions.list_sum_first_way(L))
print(list_functions.list_sum_second_way(L))


#9. Умножьте каждый элемент списка на произвольное число.

L = [12, 5, 4]
number = 4

print(list_functions.multiply_every_element_by_number(L, number))


#10. Напишите скрипт для слияния (конкатенации) двух списков тремя различ­ными способами.

L = [3, 4, 5]
M = [5, 4, 3]

print(list_functions.lists_concat_first_way(L, M))
print(list_functions.lists_concat_second_way(L, M))
print(list_functions.lists_concat_third_way(L, M))


#11. Напишите скрипт, меняющий позициями элементы списка с индек­сами n и n+1

L = [0, 1, 2, 3, 4, 5]
n = 3

print(list_functions.change_element_from_index(L, n))


#12. Напишите скрипт, переводящий список из различного количества числовых целочисленных элементов в
#    одно число. Пример списка: [1, 23, 456], результат: 123456.

L = [1, 23, 456]

print(list_functions.get_int_from_list(L))


#Найдите максимальное и минимальное числа, хранящиеся в списке

L = [1, 2, 32, 15, 44, 213, 123, 99, 852, 958, 231]

print(list_functions.find_min_and_max(L))


#Напишите скрипт, удаляющий все повторяющиеся элементы из спи­ска.

L = [1, 0, 2, 'yes', 2, 4, 'no', 2, 17, 'may be', 0, 4, 15, 'yes']

print(list_functions.delete_repeating_elements(L))


#Скопируйте список двумя различными способами.

L = [1, 0, 2]

print(list_functions.copy_list_first_way(L))
print(list_functions.copy_list_second_way(L))

