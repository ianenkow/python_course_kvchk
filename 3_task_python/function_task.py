from modules import int_and_string_functions, list_functions

#8. Напишите функцию, вычисляющую максимальное из трех чисел. Два вызова функции.

print(int_and_string_functions.find_max(1, 20, 3))
print(int_and_string_functions.find_max(-3, -5, -20))
print()


#9. Напишите функцию, которая возвращает инвертированную строку, передаваемую ей на вход.

print(int_and_string_functions.invert_string('It could be fun but it will not'))
print()    


#10. Напишите функцию для вычисления факториала задаваемого числа. Два вызова функции.

print(int_and_string_functions.calc_factorial(6))
print(int_and_string_functions.calc_factorial(2.4))
print()


#11. Напишите функцию, удаляющую повторяющиеся элементы в списке. Три вызова функции.

L1 = [1, 2, 3, 14, 2, 0, 19, 26, 288, 11, 14, 15, 0, 8, 2, 2, 3]
L2 = list('Humpty-dumpty sat on Great wall...')
L3 = ['a', 3, -8, {'h1':'n1'}, 'b', '4', 4, 3, 12, {'h1':'n1'}, 'a']

print(list_functions.del_repeats(L1))
print(list_functions.del_repeats(L2))
print(list_functions.del_repeats(L3))
print()


#12. Напишите функцию, которая проверяет, является ли строка палин­дромом (читается одинаково как
#    слева направо, так и наоборот). Два вызова функции (с палиндромом и нет).

print(int_and_string_functions.is_palindrome('Лазер Боре хер обрезал'))
print(int_and_string_functions.is_palindrome('Палиндром иначе называется "рачья песнь"'))
print()




#Почитал про декораторы и решил, что лучше оставить 13 и 14 без изменений. Вроде бы можно
#"раздекорировать" функцию, но это плохая практика, поэтому не стал углубляться


#13. Декорируйте функцию из первого упражнения таким образом, чтобы возвращаемое ей значение
#    возводилось в квадрат.

def double_decorator(func):
    def inner(x, y, z):
        return func(x, y, z) ** 2
    return inner

@double_decorator
def find_max(x, y, z):
    L = [x, y, z]
    L.sort(reverse=True)
    return L[0]

print(find_max(1, 4, 3))
print()


#14. Декорируйте функцию из 9 задания таким образом, чтобы возвращалась строка в верхнем регистре.

def upper_decorator(func):
    def inner(line):
        upper_line = str(func(line))
        return upper_line.upper()
    return inner

@upper_decorator
def invert_string(line):
    L = list(line)
    l = line[::-1]
    return ''.join(l)

s = "And here's grand finale"
print(invert_string(s))
