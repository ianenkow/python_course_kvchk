import json


class Notepad:

    def __init__(self):
        self.notes = []

    def __str__(self):
        return str(self.notes)


    def add_note(self, note):
        if len(note) == 4:
            self.notes.append(note)
        else:
            print(f'Количество данных контакта не соответствует требованию. Должно быть 4, а в передаваемом объекте {len(note)}')
    
    def add_notes(self, notes):
        if type(notes) == dict:
            self.add_note(notes)
        elif type(notes) == list:
            for note in notes:
                self.add_note(note)
    
    
    def add_from_file(self, file):
        f = open(file, 'r')
        loads = json.loads(f.read())
        for note in loads:
            self.notes.append(note)
    

    def upload_to_file(self, file_name, recording_type):
        if recording_type == 'a':
            try:
                pseudo = Notepad()
                pseudo.add_from_file(file_name)
                pseudo.add_notes(self.notes)
                self.notes = pseudo.notes
            except FileNotFoundError:
                print('Файл, куда вы пытаетесь добавить записи, отсутствует\n'\
                    'Попробуйте всопользоваться командой upload_to_file с параметром "w"')
        with open(file_name, 'w', encoding='utf8') as json_file:
            json.dump(self.notes, json_file, ensure_ascii=False, indent=2)
        

    
note = [{'ФИО':'Зубенко Михаил Петрович', 'Телефон': '+79999999999', 'Email': 'mafioznik@mail.ru', 'Дата рождения': '20.01.1974'}, {'ФИО': 'Аненков Иван Олегович', 'Телефон': '+79377387324', 'Email': '4epen@list.ru', 'Дата рождения': '20.04.1988'}]
note2 = {'ФИО': 'Иванов Иван Иванович', 'Телефон': '33-60-60', 'Email': 'ivanon@gmail.ru', 'Дата рождения': '20.10.1999'}


notepad = Notepad()
notepad.add_notes(note)
notepad.add_note(note2)
# notepad.add_note(note3)
notepad.upload_to_file('/home/repsek/python_course_kvchk/4_task_python/file.json', 'w')


notepad2 = Notepad()
notepad2.add_from_file('/home/repsek/python_course_kvchk/4_task_python/file.json')
notepad2.upload_to_file('file2.json', 'a')


