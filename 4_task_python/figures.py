from cmath import sqrt


class Figure:

    def __init__(self, side_length, side_quantity):
        self.side_length = side_length
        self.side_quantity = side_quantity
        if side_quantity == 3:
            self.type = 'Треугольник'
        elif side_quantity == 4:
            self.type = 'Квадрат'
        elif side_quantity == 6:
            self.type = 'Шестиугольник'


    def perimeter(self):
        return round((self.side_length * self.side_quantity), 2)


    def __str__(self):
        return f'{self.type}\n'\
                f'Длина стороны: {self.side_length}\n'\
                f'Периметр: {self.perimeter()}\n'\
                f'Площадь: {self.square()}\n'



class Triangle(Figure):

    def __init__(self, side_length):
        super().__init__(side_length, 3)


    def square(self):
        p = self.perimeter()
        return round(sqrt(p * (p - self.side_length)**3).real, 2)


    # def __str__(self):
    #     return f'{super().__str__()}'\
    #             f'Площадь: {self.square()}\n'



class Square(Figure):

    def __init__(self, side_length):
        super().__init__(side_length, 4)
        

    def square(self):
        return self.side_length ** 2

    # def __str__(self):
    #     return f'{super().__str__()}'\
    #             f'Площадь: {self.square()}\n'



class Hexxagon(Figure):

    def __init__(self, side_length):
        super().__init__(side_length, 6)
        

    def square(self):
        right_tri = Triangle(self.side_length)
        return round((right_tri.square() * 6), 2)


    # def __str__(self):
    #     return f'{super().__str__()}'\
    #             f'Площадь: {self.square()}\n'


fig1 = Triangle(2.7)
print(fig1)

fig2 = Square(5.5)
print(fig2)

fig3 = Hexxagon(3.2)
print(fig3)
