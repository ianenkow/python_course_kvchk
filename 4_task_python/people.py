class Human:

    def __init__(self, name, age, gender):
        self.name = name
        self.age = age
        self.gender = gender

    def __str__(self) -> str:
        return f'Объект класса {type(self).__name__}\n'\
                f'Имя: {self.name}\n'\
                f'Возраст: {self.age}\n'\
                f'Пол: {self.gender}\n'\


class Prepod(Human):
    
    def __init__(self, name, age, gender, subject, experience):
        super().__init__(name, age, gender)
        self.subject = subject
        self.experience = experience

    def __str__(self):
        return super().__str__() +\
                f'Предмет: {self.subject}\n'\
                f'Стаж: {self.experience}\n'


class Student(Human):
    
    def __init__(self, name, age, gender, specialization, course):
        super().__init__(name, age, gender)
        self.specialization = specialization
        self.course = course

    def __str__(self):
        return super().__str__() +\
                f'Специальность: {self.specialization}\n'\
                f'Курс: {self.course}\n'

ivanov = Prepod('Пётр Иванович', 53, 'М', 'История государства Российского', 24)
petrova = Prepod('Елизавета Степановна', 45, 'Ж', 'Теория государства и права', 18)
sidorov = Prepod('Клара Захаровна', 88, 'ж', 'Краткая теория всего', 88)

ivanenko = Student('Саша', 18, 'Ещё не определилось', 'Социология', 1)
petrenko = Student('Николай', 20, 'Цисгендерный мужчина', 'Социология', 4)
sidorenko = Student('Ka-52', 20, 'Боевой вертолёт', 'Воздухоплавание и космонавтика', 1)

print(ivanov)
print(petrova)
print(sidorov)
print(ivanenko)
print(petrenko)
print(sidorenko)
