class Arythmetics:
 
    def __init__(self, a, b):
        self.a = a
        self.b = b


    def sum(self):
        if type(self.a) == int and type(self.b) == int:
            return self.a + self.b
        else:
            return 'Not a number!'
           

    def difference(self):
        if type(self.a) == int and type(self.b) == int:
            return self.a - self.b
        else:
            return 'Not a number!'


    def multiplication(self):
        if type(self.a) == int and type(self.b) == int:
            return self.a * self.b
        else:
            return 'Not a number!'


    def division(self):
        if type(self.a) == int and type(self.b) == int:
            if self.b == 0:
                print('Cant divide by zero')
                self.b = 1
            return self.a / self.b
        else:
            return 'Not a number!'


    def power(self):
        if type(self.a) == int and type(self.b) == int:
            return self.a ** self.b
        else:
            return 'Not a number!'

    
    def reverse_power(self):
        if type(self.a) == int and type(self.b) == int:
            return self.b ** self.a
        else:
            return 'Not a number!'




ex1 = Arythmetics('a', 3)
print(ex1.sum())

ex2 = Arythmetics(5, 2)
print(ex2.difference())

ex3 = Arythmetics(8, 0)
print(ex3.division())

ex4 = Arythmetics(2, 2)
print(ex4.multiplication())

ex5 = Arythmetics(2, 3)
print(ex5.power())
print(ex5.reverse_power())


