import json

from jsonpath_ng.ext import parse

# 1.Вывести все цвета кейсов без дублей:
phone_json = '{ "phone": { "model": "CoolPhone", "screen": 5, "accessories": [{ "type": "phoneCase", "color": "grey" }, { "type": "phoneCase", "color": "blue" }, { "type": "phoneCase", "color": "grey" }, { "type": "phoneCase", "color": "blue" }, { "type": "ScreenProtector", "color": "noColor" }, { "type": "phoneCase", "color": "red" } ] } }'


phones = json.loads(phone_json)
jsonpath_expression = parse('$..accessories[?type = "phoneCase"].color')
phone_colors = []
for match in jsonpath_expression.find(phones):
    if not match.value in phone_colors:
        phone_colors.append(match.value)
print(phone_colors)



# 2.Вывести имя человека с самой большой суммой зарплаты и премии, и сумму:
employees_json = '{ "IT":{ "company1":{ "employee":{ "name":"Yorick", "payble":{ "salary":4356164, "bonus":51243 } } }, "company2":{ "employees":{ "first": { "name":"Biba", "zarplata":{ "salary":523761.32, "bonus":763248 }  }, "second":{ "name":"Boba", "zarplata":{ "salary":763248, "bonus":523761.32 } }, "third":{ "name":"Beba", "zarplata":{ "salary":10000, "bonus":"+40%" } } } }, "company100":{ "name":"Работяга", "zarplata":{ "salary":893423, "bonus":-123456 } } } }'


def calculate_money(salary, bonus):
    money_sum = 0
    if type(bonus) == int or type(bonus) == float:
        money_sum = salary + bonus
    if type(bonus) == str:
        bonus_rate = int(''.join([s for s in list(bonus) if s not in ['%', '+']]))/100
        money_sum = salary + salary * bonus_rate
    return money_sum


employees = json.loads(employees_json)
names_jsonpath = parse('$..name')
salaries_jsonpath = parse('$..salary')
bonuses_jsonpath = parse('$..bonus')
names = []
salaries = []
bonuses = []
for match in names_jsonpath.find(employees):
    names.append(match.value)
for match in salaries_jsonpath.find(employees):
    salaries.append(match.value)
for match in bonuses_jsonpath.find(employees):
    bonuses.append(match.value)

employee_list = []
for i in range(len(names)):
    money_sum = calculate_money(salaries[i], bonuses[i])
    current_employee = [names[i], money_sum]
    employee_list.append(current_employee)

print(sorted(employee_list, key = lambda x: x[1])[-1])








# def find_employees(json_dict: dict):
#     names = find_values(json_dict, 'name')
#     salaries = find_values(json_dict, 'salary')
#     bonuses = find_values(json_dict, 'bonus')
#     result = []

#     for i in range(len(names)):
#         employee = {}
#         employee['name'] = names[i]
#         employee['salary'] = salaries[i]
#         employee['bonus'] = bonuses[i]
#         result.append(employee)    
#     return result






# data = json.loads(employees_json)
# employees = find_employees(data)
# employee_list = []

# for i in range(len(employees)):
#     employee = (employees[i]['name'], calculate_money(employees[i]['salary'], employees[i]['bonus']))
#     employee_list.append(employee)

# result = employee_list[0][0]
# biggest_money = employee_list[0][1]
# for i in range(len(employee_list)):
#     if employee_list[i][1] > biggest_money:
#         result = employee_list[i][0]

# print(result)
